<?php

class Year
{
    public function checkYear($year){
        if(gettype($year) == "integer"){
            $str = "";
            if($year % 4 == 0) $str = " and is Bisextile";
            if($year < 2021) return "Past" . $str;
            if($year > 2021) return "Future" . $str;
            return "Present" . $str;
        }else
            return false;
    }
}
