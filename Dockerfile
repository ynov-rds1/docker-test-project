FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \
    apt-utils \
    curl \
    zip \
    unzip \    
    # Install apache
    apache2 \
    # Install php 7.2
    libapache2-mod-php7.4 \
    php7.4-cli \
    php7.4-fpm \
    php7.4-ldap \
    php7.4-mbstring \
    # Install tools
    locales \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Set locales
RUN locale-gen fr_FR.UTF-8
# Configure PHP for TYPO3
COPY docker/typo3.ini /etc/php/7.2/mods-available/
RUN phpenmod typo3
# Configure apache for TYPO3
RUN a2enmod rewrite expires
RUN echo "ServerName localhost" | tee /etc/apache2/conf-available/servername.conf
RUN a2enconf servername
# Configure vhost for TYPO3
COPY docker/typo3.conf /etc/apache2/sites-available/
RUN a2dissite 000-default
RUN a2ensite typo3.conf
# Heure et date
RUN echo "Europe/Paris" > /etc/timezone
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata
# Suppression du répertoire html
RUN rm -rf /var/www/html 
ADD src /var/www/ 

EXPOSE 80 443

CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
