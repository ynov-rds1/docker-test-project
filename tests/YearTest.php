<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/Year.php");

class YearTest extends \PHPUnit\Framework\TestCase
{
   public function testCheckYear_WithInvalidInput_ReturnsFalse()
   {
       $year = new Year();
       $result = $year->checkYear("Not a year");
       $expected = false;
       $this->assertTrue($result == $expected);
   }

   public function testCheckYear_WithBisextileYearLessThan2021_ReturnsPastAndBisextile()
   {
       $year = new Year();
       $result = $year->checkYear(1600);
       $this->expectOutputString('Past and is Bisextile');
       print $result;
   }

   public function testCheckYear_WithNonBisextileYearGreaterThan2021_ReturnsFuture()
   {
       $year = new Year();
       $result = $year->checkYear(2023);
       $this->expectOutputString('Future');
       print $result; 
   }
}
